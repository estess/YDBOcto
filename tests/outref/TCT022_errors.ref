
select (select 'a'||'b'):: integer;
select 'ab' :: integer;
select 'ab'::integer;
select '1.0'::integer;
select 't'::integer;
select 'f'::integer;
select 'test'::bool;
select 2.0::bool;
select 'a'::bool;
select -1 ::bool;
select '.1'::integer;
select '1.'::integer;
select '+1.0'::integer;
select '-1.0'::integer;
select '+1-.0'::numeric;
select ' -1. 0 '::numeric;
select '1 2'::integer;
select '              '::numeric;
select '              '::integer;
select '              '::bool;
select '.'::integer;
select '.'::numeric;
select '.  '::numeric;
select '.   '::integer;
select '.'::integer;
select '+'::integer;
select '+'::numeric;
select '+   '::integer;
select '+   '::numeric;
select true or 'a'::boolean; -- Copied from #559
select TRUE::numeric(2);
select 'abc'::numeric(2);
select 'abc'::numeric(2,1);
select '++1'::integer;
select '1--'::integer;
select '++1'::numeric;
select '1--'::numeric;
select '1.1.0'::numeric;
select '1.1.0'::numeric(1,1);
select '1.1.0'::numeric(1);
select '1.1.1'::integer;
select '+11234+1231455'::integer;
select '12112345-12'::integer;
select '-12112345-12'::integer;
select '+1231+123'::numeric;
select '1245-1123'::numeric;
select '+1231+123'::numeric;
select '-1245-1123'::numeric;

--Below queries will have NULL_VALUE type as the source type so we are not expecting it to go through the string to numerc/integer cast validation logic. Hence no errors. May change if Octo distinguishes between empty string and NULL value.
select ''::numeric;
select ''::integer;
select ''::bool;

-- Although the following is a valid query it is added to this fixture as the result is in Octo truncates the decimal portion of
--   the numeric whereas Postgres retains the numeric value as it is in the string.
select '1.0'::NUMERIC;

-- Following tests basic invalid conversions between literal types
select FALSE::numeric; -- error
select 1.1::boolean; -- error
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: 'ab'
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: 'ab'
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: 'ab'
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: '1.0'
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: 't'
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: 'f'
[ERROR]: ERR_INVALID_BOOLEAN_SYNTAX: Invalid input syntax for type boolean: 'test' is not a valid boolean value
[ERROR]: ERR_TYPE_CAST: Cannot cast type NUMERIC to type BOOLEAN
LINE 9:1: select 2.0::bool;
                 ^^^^^^^^^
[ERROR]: ERR_INVALID_BOOLEAN_SYNTAX: Invalid input syntax for type boolean: 'a' is not a valid boolean value
[ERROR]: ERR_INVALID_INPUT_SYNTAX: Invalid input syntax : Expecting type NUMERIC or INTEGER : Actual type BOOLEAN
LINE 11:1: select -1 ::bool;
                   ^
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: '.1'
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: '1.'
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: '+1.0'
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: '-1.0'
[ERROR]: ERR_INVALID_NUMERIC_SYNTAX: Invalid input syntax for type numeric: '+1-.0'
[ERROR]: ERR_INVALID_NUMERIC_SYNTAX: Invalid input syntax for type numeric: ' -1. 0 '
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: '1 2'
[ERROR]: ERR_INVALID_NUMERIC_SYNTAX: Invalid input syntax for type numeric: '              '
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: '              '
[ERROR]: ERR_INVALID_BOOLEAN_SYNTAX: Invalid input syntax for type boolean: '              ' is not a valid boolean value
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: '.'
[ERROR]: ERR_INVALID_NUMERIC_SYNTAX: Invalid input syntax for type numeric: '.'
[ERROR]: ERR_INVALID_NUMERIC_SYNTAX: Invalid input syntax for type numeric: '.  '
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: '.   '
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: '.'
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: '+'
[ERROR]: ERR_INVALID_NUMERIC_SYNTAX: Invalid input syntax for type numeric: '+'
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: '+   '
[ERROR]: ERR_INVALID_NUMERIC_SYNTAX: Invalid input syntax for type numeric: '+   '
[ERROR]: ERR_INVALID_BOOLEAN_SYNTAX: Invalid input syntax for type boolean: 'a' is not a valid boolean value
[ERROR]: ERR_TYPE_CAST: Cannot cast type BOOLEAN to type NUMERIC
LINE 32:2: select TRUE::numeric(2);
                  ^^^^^^^^^^^^^^^^
[ERROR]: ERR_INVALID_NUMERIC_SYNTAX: Invalid input syntax for type numeric: 'abc'
[ERROR]: ERR_INVALID_NUMERIC_SYNTAX: Invalid input syntax for type numeric: 'abc'
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: '++1'
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: '1--'
[ERROR]: ERR_INVALID_NUMERIC_SYNTAX: Invalid input syntax for type numeric: '++1'
[ERROR]: ERR_INVALID_NUMERIC_SYNTAX: Invalid input syntax for type numeric: '1--'
[ERROR]: ERR_INVALID_NUMERIC_SYNTAX: Invalid input syntax for type numeric: '1.1.0'
[ERROR]: ERR_INVALID_NUMERIC_SYNTAX: Invalid input syntax for type numeric: '1.1.0'
[ERROR]: ERR_INVALID_NUMERIC_SYNTAX: Invalid input syntax for type numeric: '1.1.0'
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: '1.1.1'
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: '+11234+1231455'
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: '12112345-12'
[ERROR]: ERR_INVALID_INTEGER_SYNTAX: Invalid input syntax for type integer: '-12112345-12'
[ERROR]: ERR_INVALID_NUMERIC_SYNTAX: Invalid input syntax for type numeric: '+1231+123'
[ERROR]: ERR_INVALID_NUMERIC_SYNTAX: Invalid input syntax for type numeric: '1245-1123'
[ERROR]: ERR_INVALID_NUMERIC_SYNTAX: Invalid input syntax for type numeric: '+1231+123'
[ERROR]: ERR_INVALID_NUMERIC_SYNTAX: Invalid input syntax for type numeric: '-1245-1123'
NUMERIC

(1 row)
INTEGER

(1 row)
BOOLEAN

(1 row)
NUMERIC
1
(1 row)
[ERROR]: ERR_TYPE_CAST: Cannot cast type BOOLEAN to type NUMERIC
LINE 58:1: select FALSE::numeric; -- error
                  ^^^^^^^^^^^^^^
[ERROR]: ERR_TYPE_CAST: Cannot cast type NUMERIC to type BOOLEAN
LINE 59:2: select 1.1::boolean; -- error
                  ^^^^^^^^^^^^
